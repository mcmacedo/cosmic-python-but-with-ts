# Cosmic Python but with Typescript

O propósito deste projeto é estudar o conteudo do livro [Architecture Patterns With Python](https://www.cosmicpython.com/#buy_the_book), disponível para leitura gratuita no site [Cosmic Python](https://www.cosmicpython.com/). Apesar do livro ter código e algumas implementações específicas para a linguagem Python, a parte mais importante do conteúdo não é sobre a referida linguagem, mas sim boas práticas de promgração, entre elas _TDD - Test Driven Design_ e conceitos de _DDD - Domain Driven Design_. Além disso, num momento mais adiante do conteúdo, também tras como assunto _Event Driven Microservices_.

Optei então pelo conteúdo por questões de interesse em trabalhar com Typescript.

## Pré-configurando o projeto

Este tópico especifica o que foi e como reproduzir a instalação das dependências e configurações do projeto.

### Node e Typescript

Installar o [Node.js](https://nodejs.org/en/) na máquina e instalar o [Typescript](https://www.typescriptlang.org).

Já com o Node.js instalado na máquina inicie um projeto, instale o Typescript:

```bash
npm init
npm install typescript --save-dev
```

Instale também o Node.js types para Typescript

```bash
npm install @types/node --save-dev
```

Crie na raiz do projeto, um arquivo chamado `tsconfig.json`. Nele, iremos configurar os parâmetros que desejamos no comportamento do Typescript. Para tanto, utilizamos o comando abaixo que cria o arquivo com todos os parâmetros, a maioria comentando e então descomentamos os parâmetros que necessitamos para trabalhar.

```bash
./node_modules/typescript/bin/tsc --init
```

O que cada parâmetro faz, está documentado no próprio arquivo `tsconfig.json`. Aqui serão listados quais destes foram acionados:

```json
{
  "compilerOptions": {
    "allowSyntheticDefaultImports": true,
    "baseUrl": "./",
    "esModuleInterop": true,
    "forceConsistentCasingInFileNames": true,
    "module": "commonjs",
    "moduleResolution": "node",
    "noImplicitAny": true,
    "outDir": "./dist",
    "paths": { "*": ["node_modules/*", "src/*"] },
    "strict": true,
    "target": "es2016"
  },
  "include": ["src/**/*"]
}
```

A propriedade **include** recebe um array de um gloc de arquivos que devem ser inclusos na compilação, neste caso, adicionando todos os arquivos `.ts` que estarão presentes na pasta ``/src`.

### Nodemon e scripts build, start e start:dev

Para facilitar o precesso de desenvolvimento, foram instaladas a libs `ts-node` e `nodemom` para monitorar alterações nos arquivos fonte do projeto sempre que um arquivo é salvo, assim realizando o realod do código que está sendo executado. Para tanto, também foi criado o arquivo `nodemon.js` para configurar quais devem ser as pastas e os tipos de arquivos que devem ser monitorados.

```bash
npm install --save-dev nodemon
```

Três script serão adicionados ao arquivo `package.json` na raiz do projeto para auxiliar no processo de desenvolvimento

| Nome do Script | Descrição                                                                                          |
| -------------- | -------------------------------------------------------------------------------------------------- |
| build          | transpila os arquivos `.ts` para arquivos `.js` no `outDir` configurado no arquivo `tsconfig.json` |
| start          | executa o build da aplicação e roda o javascript transpilado na pasta determinada de build         |
| start:dev      | executa a aplicação a partir do `nodemom` permitindo o code reload ao salvar                       |

### .gitignore

O arquivo `.gitignore` foi criado na raiz do projeto para evitar que arquivos e pastas que não desejamos presentes no repositório sejam adicionados aos commits.

### Jest

Para este projeto, foi utilizao o framework de testes `Jest`, sua instalação e configuração seguem abaixo:

```bash
npm install jest ts-jest @types/jest --save-dev
```

Foi criado na raiz do projeto o arquivo `jest.config.js`. Nele, instuímos o framework a consumir todos os arquivos a partir do seguinte padrão `"**/test/**/*.(test/spec).**(ts|js)"`, todos os arquivos `(.test.ts, .test.spec.ts, .test.js, .test.spec.js)` presentes na pasta `test`.

Além disso, os arquivos `.ts` serão pré-processados. Este pré-processamento apenas transpila nosso TypeScript em JavaScript utilizando as configurações presentes no arquivo `tsconfig.json`. Isto tudo acontece em memória quando rodamos os testes, assim não temos nenhuma arquivo `.js` como saída.

Para finalizar adicionar os scripts `test` e `test:dev` ao `package.json` com as seguintes finalidades:

| Nome do Script | Descrição                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------- |
| test           | roda o jest de as tags para exibir o _coverage_ do código e de forma verbosa                      |
| test:dev       | executa o script de test com a tag `--watchAll` permitindo assim o code reload no desenvolvimento |

### ESLint

Utilizaremos o conhecido ESLint para termos um padrão claro da organização do código da aplicação. Para isso instalamos o ESlint, o parser para lint de Typescript.

```bash
npm install --save-dev eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin
```

Após serem instalados as libs, iremos criar o arquivo de configuração do lint, `.eslintrc`. Também foi criado o arquivo `.eslintignore` para que as regras não sejam aplicadas em todos os arquivos de maneira irrestrita.

Para finalizar adicionar os scripts `test` e `test:dev` ao `package.json` com as seguintes finalidades:

### Prettier

Instalamos e configuramos o Prettier para trabalhar junto com ESlint, onde o lint define as convenções de código enquanto o prettier realiza a auto-formatação baseado nas regras definidas no lint. Assim, instalamos o Prettier:

```bash
npm install --save-dev prettier
```

E então criamos o arquivo `.prettierrc` com as atributos de formatação que desejamos.

```json
{
  "semi": false,
  "trailingComma": "none",
  "singleQuote": false,
  "printWidth": 120,
  "useTabs": false,
  "tabWidth": 2
}
```

`"semi": false` removerá **;** do código, isso é bem pessoal, como venho do Python, acho que se algo é desnecessário, prefiro remover para não poluir mais o código. `"trailingComma": "none"` não adicionará automaticamente **,** ao final de uma lista de parâmetros, propriedades de objetos ou itens em arrays. `"singleQuote": false` determina que strings ou propriedades acessadas com notação de colchetes `obj["propOne"]`, sejam feitas com aspas duplas. `"printWidth": 120` especifíca que o Prettier vai quebrar linhas com mais de 120 caracteres. `"useTabs": false` e `"tabWidth": 2` em conjunto definem que o prettier usurá spaces em vez de _tabs_ e que seram utilizados dois espaços para identação do código.

Agora com os parâmetros definidos, atualizamos o `package.json` com o script de formatação `prettier:format`, que busca as configurações no arquivo `.prettierrc` e aplica a formatação no path `src/**/*.ts`, com a tag `--write`, permitindo a formatação automática dos arquivos.

### Prettier + ESLint

Agora vamos juntar os dois, `Prettier` e o `ESLint`. Para isso, instalamos seguintes pacotes `eslint-config-prettier` e `eslint-plugin-prettier`. O primeiro desliga regras do Eslint que possam interferir nas regras no Prettier. O segundo transforma as regras do Prettier em regras do ESLint:

```bash
npm install --save-dev eslint-config-prettier eslint-plugin-prettier
```

Agora, fazemos alguns ajustes no `.eslintrc`, adicionando o `prettier` aos _plugins_ e ao _extends_ e nas _rules_, damos ao prettier o valor **2**, indicando que as regras do prettier serão avaliadas como erro para o ESlint.

Pronto, agora quando alguma regra do Prettier não for respeitada ao rodar o script de lint, um erro será informado e então basta rodar o script `prettier:format` para corrigi-las.
